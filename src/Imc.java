import java.util.Scanner;

public class Imc {
    public static void main(String[] args) {
        int opcao;
        double altura;
        double peso;
        double imc;

        System.out.println("******** Software Para cálculo do índice de massa corporal ********");


        System.out.println("Selecione uma Opção:");

        System.out.println("1 - Homem");
        System.out.println("2 - Mulher");
        System.out.println("3 - Sair");


        Scanner menu = new Scanner(System.in);
        opcao = menu.nextInt();


        Scanner entrada = new Scanner(System.in);
        switch (opcao) {
            case 1: {
                System.out.println("Opção Selecionada Homen:\n");
                System.out.println("Digite sua Altura");
                altura = entrada.nextDouble();

                System.out.println("Digite seu Peso");
                peso = entrada.nextDouble();

                imc = peso / (altura * altura);

                if (imc < 20) {
                    System.out.println("Seu IMC é igual a: " + imc + " Você esta abaixo do peso");
                } else if (imc >= 20 && imc <= 24.9) {
                    System.out.println("Seu IMC é igual a: " + imc + " Você esta no peso ideal");
                } else if (imc >= 25 && imc <= 29.9) {
                    System.out.println("Seu IMC é igual a: " + imc + " Você esta com obesidade leve");
                } else if (imc >= 30 && imc <= 39.9) {
                    System.out.println("Seu IMC é igual a: " + imc + " Você esta com obesidade moderada");
                } else {
                    System.out.println("Seu IMC é igual a: " + imc + " Você esta com obesidade mórbida");
                }
                break;
            }
            case 2: {
                System.out.println("Opção Selecionada Mulher:\n");

                System.out.println("Digite sua Altura");
                altura = entrada.nextDouble();

                System.out.println("Digite seu Peso");
                peso = entrada.nextDouble();

                imc = peso / (altura * altura);

                if (imc < 19) {
                    System.out.println("Seu IMC é igual a: " + imc + " Você esta abaixo do peso");
                } else if (imc >= 19 && imc <= 23.9) {
                    System.out.println("Seu IMC é igual a: " + imc + " Você esta no peso ideal");
                } else if (imc >= 24 && imc <= 28.9) {
                    System.out.println("Seu IMC é igual a: " + imc + " Você esta com obesidade leve");
                } else if (imc >= 29 && imc <= 38.9) {
                    System.out.println("Seu IMC é igual a: " + imc + " Você esta com obesidade moderada");
                } else {
                    System.out.println("Seu IMC é igual a: " + imc + " Você esta com obesidade mórbida");
                }
                break;

            }
            case 3: {
                System.out.println("Obrigado por utilizar nosso software!! copyright Oseas Junior, agradecimentos equipe Mentorama ");
            }
        }
    }
}
